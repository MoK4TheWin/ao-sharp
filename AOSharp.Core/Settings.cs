﻿using AOSharp.Common.Unmanaged.DataTypes;
using AOSharp.Common.Unmanaged.Interfaces;
using AOSharp.Core.UI;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace AOSharp.Core
{
    public class Settings
    {
        private readonly string _groupName;
        private readonly string _savePath;
        private List<string> _variables;
        private Dictionary<string, string> _storedValues;

        public Variant this[string name]
        {
            get
            {
                if (!_variables.Contains(name))
                    return null;

                return DistributedValue.GetDValue($"{_groupName}__{name}", false);
            }

            set
            {
                DistributedValue.SetDValue($"{_groupName}__{name}", value);
            }
        }

        public Settings(string groupName)
        {
            _groupName = groupName;
            _savePath = $"{Preferences.GetCharacterPath()}\\AOSharp\\{groupName}.config";
            Load();
        }

        private void AddVariable(string name)
        {
                _variables.Add(name);

                if (_storedValues.ContainsKey(name))
                    this[name] = Variant.LoadFromString(_storedValues[name]);
        }

        public void AddVariable(string name, int value)
        {
            if (!_variables.Contains(name))
            {
                DistributedValue.Create($"{_groupName}__{name}", value);
                AddVariable(name);
            }
        }

        public void AddVariable(string name, float value)
        {
            if (!_variables.Contains(name))
            {
                DistributedValue.Create($"{_groupName}__{name}", value);
                AddVariable(name);
            }
        }

        public void AddVariable(string name, bool value)
        {
            if (!_variables.Contains(name))
            {
                DistributedValue.Create($"{_groupName}__{name}", value);
                AddVariable(name);
            }
        }

        public bool IsEnabled(string name)
        {
            Variant value = this[name];
            return value != null && value.AsBool();
        }

        public void Save()
        {
            Chat.WriteLine("Saving " + _groupName);
            Dictionary<string, string> values = new Dictionary<string, string>();

            foreach(string variable in _variables)
                values.Add(variable, this[variable].ToString());

            new FileInfo(_savePath).Directory.Create(); //Create folder if it doesn't exist
            File.WriteAllText(_savePath, JsonConvert.SerializeObject(values, Formatting.Indented));
        }

        public void Load()
        {
            try
            {
                Dictionary<string, string> loadedSettings = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(_savePath));
                foreach(string settingName in loadedSettings.Keys)
                {
                    string settingValue = loadedSettings[settingName];
                    DistributedValue.Create($"{_groupName}__{settingName}", Variant.LoadFromString(settingValue));
                }
                _storedValues = loadedSettings;
                _variables = new List<string>(_storedValues.Keys);
            } 
            catch 
            {
                _storedValues = new Dictionary<string, string>();
                _variables = new List<string>();
            }
        }
    }
}
